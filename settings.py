from os import getenv

HOST = getenv("BE_CONF_HOST", "0.0.0.0")
PORT = int(getenv("BE_CONF_PORT", 5000))

SOCKETIO_HOST = getenv("BE_CONF_SOCKETIO_HOST", "http://localhost:5102")

MONGO_HOST = getenv("BE_CONF_MONGO_HOST", "localhost")
MONGO_PORT = int(getenv("BE_CONF_MONGO_PORT", 27017))
MONGO_USER = getenv("BE_CONF_MONGO_USER", "root")
MONGO_PASSWORD = getenv("BE_CONF_MONGO_PASSWORD", "pass12345")

REDIS_HOST = getenv("BE_CONF_REDIS_HOST", "localhost")
REDIS_PORT = int(getenv("BE_CONF_REDIS_PORT", 6379))

DATA_CACHE_TIME = 60 * 60 * 6