from cachetools.func import ttl_cache

from db import db

import datetime
import json
import logging
import time
import uuid

import pandas as pd
import pymongo
from bson import ObjectId
from flask import jsonify, request
from flask_cors import cross_origin
from bson import ObjectId
import enum
import settings
import unidecode

class CovidLevel(enum.Enum):
    CAP_1 = "#3BD248"
    CAP_2 = "#D4D727"
    CAP_3 = "#F21115"
    CAP_4 = "#9F0D95"

def register_route(app):
    @app.route("/", methods=["GET", "POST"])
    @cross_origin()
    def index():
        return "index"

    @app.route("/addNewPatient", methods=["POST"])
    @cross_origin()
    def addNewPatient():
        print(request.json)
        if request.json is not None:
            data = request.json
            data["_id"] = ObjectId()
            db["PATIENT"].insert_one(data)
            return {"msg":"add sucesss","data":None},200
        else:
            return {"msg": "Bad request", "data": None}, 401

    @ttl_cache(maxsize=128, ttl=settings.DATA_CACHE_TIME)
    @app.route("/getAllPatient", methods=["GET", "POST"])
    @cross_origin()
    def getAllPatient():
        allPa = db["PATIENT"].find()
        return jsonify(allPa)

    @app.route("/getPatientStatus", methods=["GET", "POST"])
    @cross_origin()
    def getPatientStatus():
        if request.json!= None and "dateTime" in request.json and request.json["dateTime"]!="":
            countPa = list(db["PATIENT"].aggregate([
                { "$match": {"ngay_cong_bo": {"$regex":request.json["dateTime"]}}},
                {"$group": {"_id": "$tinh_trang", "count": {"$sum": 1}}}
            ]))
        else:
            countPa = list(db["PATIENT"].aggregate([{"$group": {"_id": "$tinh_trang", "count": {"$sum": 1}}}]))
        tmp = {}
        for item in countPa:
            if "Đang điều trị" in item["_id"]:
                if "Đang điều trị" not in tmp:
                    tmp["Đang điều trị"] = item["count"]
                else:
                    tmp["Đang điều trị"]+= item["count"]
            else:
                tmp[item["_id"].replace("\n","").strip()]=item["count"]
        output = [{"name":k,"value":v} for k,v in tmp.items()]
        return jsonify(output)

    @ttl_cache(maxsize=128, ttl=settings.DATA_CACHE_TIME)
    @app.route("/getPatientGroupArea", methods=["GET", "POST"])
    @cross_origin()
    def getPatientGroupArea():
        alowArea = ["ngu hanh son","son tra","cam le","hoa vang","lien chieu"]
        if request.json!= None and "dateTime" in request.json and request.json["dateTime"]!="":
            countPa = list(db["PATIENT"].aggregate([
                { "$match": {"ngay_cong_bo": {"$regex":request.json["dateTime"]}}},
                {"$group": {"_id": "$quan_huyen", "count": {"$sum": 1}}}
            ]))
        else:
            countPa = list(db["PATIENT"].aggregate([{"$group": {"_id": "$quan_huyen", "count": {"$sum": 1}}}]))
        sumSetPa = {}
        for dis in countPa:
            print(dis)
            if dis["_id"] == "" or dis["_id"] == None:
                continue

            if unidecode.unidecode(dis["_id"].lower()).strip() not in sumSetPa:
                try:
                    sumSetPa[unidecode.unidecode(dis["_id"].lower()).strip()] = dis["count"]
                except Exception as e:
                    print(e)
            else:
                try:
                    sumSetPa[unidecode.unidecode(dis["_id"].lower()).strip()] += dis["count"]
                except Exception as e:
                    print(e)
        output = []
        for k,v in sumSetPa.items():
            if v<10:
                continue
            output.append({"name":k,"value":v})

        return jsonify(output)

    @app.route("/getRegionStatusMap", methods=["GET", "POST"])
    @cross_origin()
    @ttl_cache(maxsize=128, ttl=settings.DATA_CACHE_TIME)
    def getRegionStatusMap():
        listRegion = list(db["REGION"].find({},{"_id":0}))
        dateTime = ""
        try:
            if "dateTime" in request.json:
                dateTime = request.json["dateTime"]
        except:
            pass
        for region in listRegion:

            regionPA_KB = db["PATIENT"].find({
                "quan_huyen":{"$regex":region["filterValue"]},
                "tinh_trang":{"$regex":"khỏi bệnh"},
                "ngay_cong_bo":{"$regex":dateTime}},{"_id":0})
            regionPA_TV = db["PATIENT"].find({
                "quan_huyen":{"$regex":region["filterValue"]},
                "tinh_trang":{"$regex":"Tử vong"},
                "ngay_cong_bo":{"$regex":dateTime}},{"_id":0})
            regionPA_DDT = db["PATIENT"].find({
                "quan_huyen":{"$regex":region["filterValue"]},
                "tinh_trang":{"$regex":"Đang điều trị"},
                "ngay_cong_bo":{"$regex":dateTime}},{"_id":0})
            region["pa_status"] = {"DDT":regionPA_DDT.count(),"TV":regionPA_TV.count(),"KB":regionPA_KB.count()}
            if regionPA_DDT.count() < 25:
                region["color_hex_background"] = CovidLevel.CAP_1.value
            if 25 <= regionPA_DDT.count() <= 50:
                region["color_hex_background"] = CovidLevel.CAP_2.value
            if 50 <= regionPA_DDT.count() <= 75:
                region["color_hex_background"] = CovidLevel.CAP_3.value
            if 100 <= regionPA_DDT.count():
                region["color_hex_background"] = CovidLevel.CAP_4.value
            if region["filterValue"] == "Hải Châu":
                print(region)
        return jsonify(listRegion)



    @app.route("/get_pa_by_province", methods=["GET", "POST"])
    @cross_origin()
    @ttl_cache(maxsize=128, ttl=settings.DATA_CACHE_TIME)
    def get_pa_by_province():
        data = (db["PATIENT"].find({},{"ngay_cong_bo":1,"_id":0}))
        df = pd.DataFrame(data)
        output = []
        for name, group in df.groupby("ngay_cong_bo"):
            output.append({"dateTime":name.replace("T00:00:00",""), "count":len(group)})
        return {"data":output, "msg":"fetch data success!"}, 200
    @app.route("/getDistrictStatusMap", methods=["GET", "POST"])
    @cross_origin()
    @ttl_cache(maxsize=128, ttl=settings.DATA_CACHE_TIME)
    def getDistrictStatusMap():
        listRegion = list(db["DISTRICT"].find({}, {"_id": 0}))
        dateTime = ""
        baseQuery = {}
        try:
            if "dateTime" in request.json:
                dateTime = request.json["dateTime"]
                baseQuery = {"ngay_cong_bo": {"$regex": dateTime}}
        except:
            pass

        for region in listRegion:
            if dateTime!="":
                regionPA_KB = db["PATIENT"].find({
                    "dia_chi": {"$regex": region["xa_phuong"].strip()},
                    "$or":[{"tinh_trang": {"$regex": "khỏi bệnh"}},{"tinh_trang": {"$regex": "Đã khỏi"}}],
                    "ngay_cong_bo": {"$regex": dateTime}}, {"_id": 0})
                regionPA_TV = db["PATIENT"].find({
                    "dia_chi": {"$regex": region["xa_phuong"].strip()},
                    "tinh_trang": {"$regex": "Tử vong"},
                    "ngay_cong_bo": {"$regex": dateTime}}, {"_id": 0})
                regionPA_DDT = db["PATIENT"].find({
                    "dia_chi": {"$regex": region["xa_phuong"].strip()},
                    "tinh_trang": {"$regex": "điều trị"},
                    "ngay_cong_bo": {"$regex": dateTime}}, {"_id": 0})
                region["pa_status"] = {"DDT": regionPA_DDT.count(), "TV": regionPA_TV.count(), "KB": regionPA_KB.count()}
                if regionPA_DDT.count() < 5:
                    region["ma_mau"] = CovidLevel.CAP_1.value
                if 5 <= regionPA_DDT.count() <= 10:
                    region["ma_mau"] = CovidLevel.CAP_2.value
                if 10 <= regionPA_DDT.count() <= 15:
                    region["ma_mau"] = CovidLevel.CAP_3.value
                if 15 <= regionPA_DDT.count():
                    region["ma_mau"] = CovidLevel.CAP_4.value
            else:
                regionPA_KB = db["PATIENT"].find({
                    "dia_chi": {"$regex": region["xa_phuong"].strip()},
                    "$or": [{"tinh_trang": {"$regex": "khỏi bệnh"}}, {"tinh_trang": {"$regex": "Đã khỏi"}}],
                    }, {"_id": 0})
                regionPA_TV = db["PATIENT"].find({
                    "dia_chi": {"$regex": region["xa_phuong"].strip()},
                    "tinh_trang": {"$regex": "Tử vong"},
                    }, {"_id": 0})
                regionPA_DDT = db["PATIENT"].find({
                    "dia_chi": {"$regex": region["xa_phuong"].strip()},
                    "tinh_trang": {"$regex": "điều trị"},
                    }, {"_id": 0})
                region["pa_status"] = {"DDT": regionPA_DDT.count(), "TV": regionPA_TV.count(),
                                       "KB": regionPA_KB.count()}
                if regionPA_DDT.count() < 5:
                    region["ma_mau"] = CovidLevel.CAP_1.value
                if 5 <= regionPA_DDT.count() <= 10:
                    region["ma_mau"] = CovidLevel.CAP_2.value
                if 10 <= regionPA_DDT.count() <= 15:
                    region["ma_mau"] = CovidLevel.CAP_3.value
                if 15 <= regionPA_DDT.count():
                    region["ma_mau"] = CovidLevel.CAP_4.value
        return jsonify(listRegion)
