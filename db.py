import pymongo

import settings

# mongoClient = pymongo.MongoClient(
#     f"mongodb://{settings.MONGO_USER}:{settings.MONGO_PASSWORD}"
#     f"@{settings.MONGO_HOST}:{settings.MONGO_PORT}/?authSource=admin"
# )
# mongoClient = pymongo.MongoClient(
#     f"mongodb://{settings.MONGO_USER}:{settings.MONGO_PASSWORD}"
#     f"@{settings.MONGO_HOST}:{settings.MONGO_PORT}/?authSource=admin"
# )

mongoClient = pymongo.MongoClient(
    f"mongodb://"
    f"localhost:27017/?authSource=admin"
)
db = mongoClient["covidmap"]
print(db.list_collection_names())