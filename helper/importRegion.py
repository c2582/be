import pymongo
import json
import unidecode
mongoClient = pymongo.MongoClient(
    f"mongodb://"
    f"localhost:27017/?authSource=admin"
)
db = mongoClient["covidmap"]
areas = json.load(open("../data/dnmap/areas.json",encoding="utf-8"))
print(areas)
db["REGION"].insert_many(areas)