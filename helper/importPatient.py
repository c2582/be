import pymongo
import json
import unidecode
import datetime
mongoClient = pymongo.MongoClient(
    f"mongodb://"
    f"localhost:27017/?authSource=admin"
)
db = mongoClient["covidmap"]
pa = json.load(open("../data/dn.json",encoding="utf-8"))
for item in pa:
    item["ngay_cong_bo"] = datetime.datetime.strptime(item["ngay_cong_bo"],"%d/%m/%Y").isoformat()
print(pa)
db["PATIENT"].insert_many(pa)

# countPa = list(db["PATIENT"].aggregate([{"$group" : {"_id":"$quan_huyen", "count":{"$sum":1}}}]))
# sumSetPa = {}
# for dis in countPa:
#     if dis["_id"]=="":
#         continue
#     if unidecode.unidecode(dis["_id"].lower()).strip() not in sumSetPa:
#         sumSetPa[unidecode.unidecode(dis["_id"].lower()).strip()] = dis["count"]
#     else:
#         sumSetPa[unidecode.unidecode(dis["_id"].lower()).strip()]+=dis["count"]
# print(sumSetPa)