import pymongo
import json
import unidecode
mongoClient = pymongo.MongoClient(
    f"mongodb://"
    f"localhost:27017/?authSource=admin"
)
db = mongoClient["covidmap"]
districtAreas = json.load(open("../data/dnmap/districtAreas.json",encoding="utf-8"))
print(districtAreas)

db["DISTRICT"].insert_many(districtAreas)