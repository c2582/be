import pymongo
import json
import unidecode
mongoClient = pymongo.MongoClient(
    f"mongodb://"
    f"localhost:27017/?authSource=admin"
)
db = mongoClient["covidmap"]
point = json.load(open("../data/dnmap/pois.json",encoding="utf-8"))
print(point)
db["POINT"].insert_many(point)